mod brokers;
mod config;
mod dto;
mod filters;
mod handlers;
mod messages;

use envconfig::Envconfig;
use std::net::Ipv4Addr;
use warp::Filter;

use config::{log::init_log, Config};

#[tokio::main]
async fn main() {
    let config = Config::init_from_env().unwrap();

    init_log(config.log_level.clone());

    let api = filters::main(config.clone());

    let routes = api.with(warp::log("warp"));

    let host: Ipv4Addr = config.host.parse().unwrap();
    let port: u16 = config.port;

    warp::serve(routes).run((host, port)).await;
}
