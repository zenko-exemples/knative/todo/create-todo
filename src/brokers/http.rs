use async_trait::async_trait;
use cloudevents::{binding::reqwest::RequestBuilderExt, Event};

use super::Broker;
use crate::config::Config;

#[derive(Clone)]
pub struct HttpBroker {
    uri: String,
}

#[async_trait]
impl Broker for HttpBroker {
    type Error = String;

    fn new(config: Config) -> Self {
        HttpBroker {
            uri: config.broker.uri.expect("Broker URI"),
        }
    }

    async fn send(self, event: Event) -> Result<(), Self::Error> {
        log::debug!(target: "broker", "HTTP BROKER");

        let client = reqwest::Client::new();

        let request = client.post(self.uri).event(event);

        if let Err(error) = request {
            log::error!(target: "broker", "\t->Error when serializing event: {:?}", error);
            return Err(error.to_string());
        }

        let response = request.unwrap().send().await;

        match response {
            Ok(_) => Ok(()),
            Err(error) => {
                log::error!(target: "broker", "\t->Error when sending the event: {:?}", error);
                return Err(error.to_string());
            }
        }
    }
}
