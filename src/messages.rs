use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct Message {
    pub id: String,
    pub title: String,
    pub description: Option<String>,
}
