use cloudevents::{EventBuilder, EventBuilderV10};
use std::convert::Infallible;
use uuid::Uuid;
use warp::hyper::StatusCode;

use crate::{brokers::Broker, config::Config, dto::CreateTodo, messages::Message};

pub async fn handle(
    _config: Config,
    broker: impl Broker,
    body: CreateTodo,
) -> Result<impl warp::Reply, Infallible> {
    log::debug!(target: "handler", "Handle request");

    let id = Uuid::new_v4();

    let event = EventBuilderV10::new()
        .id(id.to_string())
        .ty("TODO_CREATED")
        .source("http://localhost/")
        .data(
            "application/json",
            serde_json::to_string(&Message {
                id: id.to_string(),
                title: body.title.clone(),
                description: body.description.clone(),
            })
            .unwrap(),
        )
        .build()
        .unwrap();

    log::debug!(target: "handler", "\t-> Event: {:?}", event);

    let res = broker.send(event).await;

    match res {
        Ok(_) => Ok(StatusCode::NO_CONTENT),
        Err(error) => {
            log::debug!(target: "handler", "\t->Error when sending the event: {:?}", error);
            return Ok(StatusCode::INTERNAL_SERVER_ERROR);
        }
    }
}
