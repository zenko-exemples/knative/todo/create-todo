pub mod broker;
pub mod log;

use envconfig::Envconfig;

use self::{broker::Broker, log::LogLevel};

#[derive(Envconfig, Clone)]
pub struct Config {
    #[envconfig(from = "PORT", default = "8080")]
    pub port: u16,

    #[envconfig(from = "HOST", default = "127.0.0.1")]
    pub host: String,

    #[envconfig(nested = true)]
    pub log_level: LogLevel,

    #[envconfig(nested = true)]
    pub broker: Broker,
}
