use envconfig::Envconfig;
use log::LevelFilter;
use pretty_env_logger::env_logger::Builder;

#[derive(Envconfig, Clone)]
pub struct LogLevel {
    #[envconfig(from = "WARP_LOG", default = "info")]
    pub warp: LevelFilter,

    #[envconfig(from = "HANDLER_LOG", default = "info")]
    pub handler: LevelFilter,

    #[envconfig(from = "BROKER_LOG", default = "info")]
    pub broker: LevelFilter,
}

pub fn init_log(level: LogLevel) {
    Builder::new()
        .filter(Some("warp"), level.warp)
        .filter(Some("handler"), level.handler)
        .filter(Some("broker"), level.broker)
        .init();
}
